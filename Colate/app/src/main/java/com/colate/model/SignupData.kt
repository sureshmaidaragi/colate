package com.colate.model

import java.io.Serializable

/**
 * Created by sureshmaidaragi on 09/06/18.
 */
class SignupData(name: String, email: String, pass: String, confPass: String, phone: String) : Serializable {

    val name: String? = name
    val email: String? = email
    val pass: String? = pass
    val confPass: String? = confPass
    val phone: String? = phone

}
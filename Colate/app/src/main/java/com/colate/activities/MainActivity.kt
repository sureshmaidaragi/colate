package com.colate.activities

import android.content.Intent
import android.os.Bundle
import android.support.design.widget.NavigationView
import android.support.v4.view.GravityCompat
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import com.colate.R
import com.colate.adapters.FolderFilesAdapter
import com.colate.model.FolderFilesModel
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*

class MainActivity : BaseActivity(), NavigationView.OnNavigationItemSelectedListener {

    // Initializing an empty ArrayList to be filled with Sample data
    val tempDataList: ArrayList<FolderFilesModel> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setSupportActionBar(toolbar)


        val toggle = ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close)
        drawer_layout.addDrawerListener(toggle)
        toggle.syncState()

        nav_view.setNavigationItemSelectedListener(this)

        createJSON()
        // Creates a vertical Layout Manager
        data_list.layoutManager = LinearLayoutManager(this)
        data_list.adapter = FolderFilesAdapter(tempDataList, this)

        create_folder_fab.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val intent = Intent(applicationContext, CreateFolderActivity::class.java)
                startActivity(intent)
            }
        })
    }

    override fun onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }


    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        when (item.itemId) {
            R.id.action_settings -> return true
            else -> return super.onOptionsItemSelected(item)
        }
    }

    override fun onNavigationItemSelected(item: MenuItem): Boolean {
        // Handle navigation view item clicks here.
        when (item.itemId) {
            R.id.nav_camera -> {
                // Handle the camera action
            }
            R.id.nav_gallery -> {

            }
            R.id.nav_slideshow -> {

            }
            R.id.nav_manage -> {

            }
            R.id.nav_share -> {

            }
            R.id.nav_send -> {

            }
        }

        drawer_layout.closeDrawer(GravityCompat.START)
        return true
    }

    fun createJSON() {

        val folderFilesModel: FolderFilesModel? = FolderFilesModel("shared", "8 Jun 2018", "Agra Trip")
        tempDataList.add(folderFilesModel!!)
        val folderFilesModel1: FolderFilesModel? = FolderFilesModel("shared", "7 Jun 2018", "Chikmagalur")
        tempDataList.add(folderFilesModel1!!)

        val folderFilesModel2: FolderFilesModel? = FolderFilesModel("no", "6 Jun 2018", "colg intern IC")
        tempDataList.add(folderFilesModel2!!)
        val folderFilesModel3: FolderFilesModel? = FolderFilesModel("no", "26 May 2018", "Contacts")
        tempDataList.add(folderFilesModel3!!)
        val folderFilesModel4: FolderFilesModel? = FolderFilesModel("no", "23 Apr 2018", "Documents")
        tempDataList.add(folderFilesModel4!!)


        val folderFilesModel5: FolderFilesModel? = FolderFilesModel("no", "28 Mar 2018", "flickers tag")
        tempDataList.add(folderFilesModel5!!)
        val folderFilesModel6: FolderFilesModel? = FolderFilesModel("no", "16 Mar 2018", "Project papers")
        tempDataList.add(folderFilesModel6!!)

        val folderFilesModel7: FolderFilesModel? = FolderFilesModel("no", "6 Mar 2018", "Project documents")
        tempDataList.add(folderFilesModel7!!)
        val folderFilesModel8: FolderFilesModel? = FolderFilesModel("no", "5 Mar 2018", "Colat test")
        tempDataList.add(folderFilesModel8!!)
        val folderFilesModel9: FolderFilesModel? = FolderFilesModel("no", "4 Mar 2018", "AI tools used")
        tempDataList.add(folderFilesModel9!!)
        val folderFilesModel10: FolderFilesModel? = FolderFilesModel("no", "3 Mar 2018", "Machine learning books")
        tempDataList.add(folderFilesModel10!!)
        val folderFilesModel11: FolderFilesModel? = FolderFilesModel("no", "2 Mar 2018", "Offers list")
        tempDataList.add(folderFilesModel11!!)
        val folderFilesModel12: FolderFilesModel? = FolderFilesModel("no", "1 Mar 2018", "Estimations")
        tempDataList.add(folderFilesModel12!!)
        val folderFilesModel13: FolderFilesModel? = FolderFilesModel("no", "28 Feb 2018", "Algorithms")
        tempDataList.add(folderFilesModel13!!)
        // val folderFilesModel8: FolderFilesModel? = FolderFilesModel("shared", "1 Jan 2018", "New year party")
        //  tempDataList.add(folderFilesModel8!!)


    }
}



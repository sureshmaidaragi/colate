package com.colate.activities

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.view.View
import com.colate.R
import com.colate.utils.PermissionUtils
import kotlinx.android.synthetic.main.registration_activity.*
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*


/**
 * Created by sureshmaidaragi on 09/06/18.
 */
class RegistrationActivity : BaseActivity() {
    val REQUEST_IMAGE_CAPTURE = 1
    companion object {
        var imageBitmap: Bitmap ?=null

    }
    var mCurrentPhotoPath: String? = null
    var mContext: Context? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.registration_activity)
        mContext = applicationContext
        signup_btn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                val intent = Intent(applicationContext, CreateFolderActivity::class.java)
                val bundle = Bundle()
                bundle.putString("name", name_edt.text.toString())
                bundle.putString("email", email_edt.text.toString())
                bundle.putString("phone", phone_edt.text.toString())
                 intent.putExtra("signup_data", bundle)
                startActivity(intent)
            }
        })
        upload_pic.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                if (PermissionUtils.requestPermission(
                                this@RegistrationActivity,
                                REQUEST_IMAGE_CAPTURE,
                                Manifest.permission.READ_EXTERNAL_STORAGE,
                                Manifest.permission.CAMERA)) {
                    dispatchTakePictureIntent()
                } else {

                }
            }
        })
    }

    private fun dispatchTakePictureIntent() {
        val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (takePictureIntent.resolveActivity(packageManager) != null) {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
        }

        /*val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        // Ensure that there's a camera activity to handle the intent
        if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            var photoFile: File? = null
            try {
                photoFile = createImageFile()
            } catch (ex: IOException) {
                // Error occurred while creating the File...
            }

            // Continue only if the File was successfully created
            if (photoFile != null) {

                val photoURI = FileProvider.getUriForFile(applicationContext, this.getApplicationContext().getPackageName() + ".provider", createImageFile())

                takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT,photoURI)
                startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE)
            }
        }*/
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        // Create an image file name
        val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss").format(Date())
        val imageFileName = "JPEG_" + timeStamp + "_"
        val storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        val image = File.createTempFile(
                imageFileName, /* prefix */
                ".jpg", /* suffix */
                storageDir      /* directory */
        )

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = image.absolutePath
        return image
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == Activity.RESULT_OK) {
            val extras = data.extras
            imageBitmap = extras!!.get("data") as Bitmap
            upload_pic.setImageBitmap(imageBitmap)

            //val file = File(mCurrentPhotoPath)

            //  val uri1 = Uri.fromFile(file)
            //  println("mCurrentPhotoPath___" + mCurrentPhotoPath)


        }
    }


}
package com.colate.activities

import android.os.Bundle
import android.view.View
import com.colate.R
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.create_folder_activity.*

class CreateFolderActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.create_folder_activity)
        setSupportActionBar(toolbar)

        try {
            val data: Bundle = intent.getBundleExtra("signup_data")
            name_tv.text = data.getString("name")
            email_tv.text = data.getString("email")
            phone_tv.text = data.getString("phone")
            content_view.visibility = View.VISIBLE
            selected_img.setImageBitmap(RegistrationActivity.imageBitmap)

        } catch (e: Exception) {
            content_view.visibility = View.GONE
        }


        //data.getSerializable("signup_data")
        // val signup_list = data.getSerializable("value") as List<SignupData>

        back_btn.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                finish()
            }
        })

    }
}
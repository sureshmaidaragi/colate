package com.colate.activities

import android.support.v7.app.AppCompatActivity


//parent activity for the activities, which holds the toast messages, snackbar actions, network,util functions
open class BaseActivity : AppCompatActivity() {
}
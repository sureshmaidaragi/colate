package com.colate.adapters

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import com.colate.R
import com.colate.activities.CreateFolderActivity
import com.colate.model.FolderFilesModel
import kotlinx.android.synthetic.main.folderfile_row_item.view.*


class FolderFilesAdapter(val items: ArrayList<FolderFilesModel>, val context: Context) : RecyclerView.Adapter<ViewHolder>() {

    // Gets the number of Data in the list
    override fun getItemCount(): Int {
        return items.size
    }

    // Inflates the item views
    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context).inflate(R.layout.folderfile_row_item, parent, false))
    }

    // Binds each Data in the ArrayList to a view
    override fun onBindViewHolder(holder: ViewHolder?, position: Int) {

        if (items.get(position).dataType.equals("shared", true)) {

            holder?.dataType?.setImageDrawable(context.resources.getDrawable(R.drawable.ic_round_folder_shared))
            holder?.dataModifiedDate?.text = "Modified: " + items.get(position).dataDateModified
            holder?.dataModifiedDate?.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_baseline_supervisor_account, 0, 0, 0);


        } else {
            holder?.dataType?.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_folder))
            holder?.dataModifiedDate?.text = "Modified: " + items.get(position).dataDateModified
            holder?.dataModifiedDate?.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);


        }

        holder?.contentRlt?.setOnClickListener(object : View.OnClickListener {
            override fun onClick(v: View?) {
                navCreateFolder(items.get(position).dataTitle, context)

            }
        })
        holder?.dataTitle?.text = items.get(position).dataTitle
    }

    fun navCreateFolder(dataTitle: String?, context: Context) {
        Toast.makeText(context, "Clicked on " + dataTitle, Toast.LENGTH_SHORT).show()
        val intent = Intent(context, CreateFolderActivity::class.java)
        context.startActivity(intent)

    }
}

class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    // Holds the TextView that will add each Data to
    val dataType = view.data_type
    val dataTitle = view.data_title
    val dataModifiedDate = view.data_modified_date
    val contentRlt = view.content_rlt
}